import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ColumnsModule } from './columns/columns.module';
import { CardsModule } from './cards/cards.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Column } from './columns/entities/column.entity';
import { Card } from './cards/entities/card.entity';
import { DB_URL } from './config';

@Module({
  imports: [ColumnsModule, 
    CardsModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      url: DB_URL,
      entities: [Column, Card],
      synchronize: true,
      logging: true,
    })
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
